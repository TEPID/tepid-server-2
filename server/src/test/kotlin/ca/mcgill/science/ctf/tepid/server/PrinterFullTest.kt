package ca.mcgill.science.ctf.tepid.server

import ca.allanwang.kit.logger.WithLogging
import ca.mcgill.science.ctf.tepid.server.internal.*
import ca.mcgill.science.ctf.tepid.server.models.*
import ca.mcgill.science.ctf.tepid.server.tables.*
import ca.mcgill.science.ctf.tepid.server.utils.Printer
import org.jetbrains.exposed.sql.deleteAll
import org.jetbrains.exposed.sql.selectAll
import org.jetbrains.exposed.sql.transactions.transaction
import org.junit.AfterClass
import org.junit.BeforeClass
import org.junit.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue
import kotlin.test.fail

/**
 * Tests for the full printing lifecycle
 */
class PrinterFullTest {

    companion object : WithLogging() {
        @BeforeClass
        @JvmStatic
        fun before() {
            TestConfigs.setup()
            transaction {
                stdlog()
                Queues.deleteAll()
                Destinations.deleteAll()
                val queue = QueueDb.new(testQueue) {
                    name = "Test Queue"
                }

                destinations.forEach {
                    DestinationDb.new(it) {
                        name = it
                        this.queue = queue
                        up = it in upDestinations
                    }
                }
                log.info("Created destinations: ${Destinations.selectAll().joinToString("\n\t", prefix = "\n\t")}")
                log.info("Finished set up\n\n")
            }
        }

        const val testQueue = "test_queue"
        val destinations = (0..5).map { "destination$it" }
        val upDestinations = (0..5 step 2).map { "destination$it" }

        private val testValidator: Validator<PrintRequest> = {
            with(it) {
                when {
                    destination !in upDestinations -> Invalid("Destination $destination not valid")
                    psInfo.pages == 0 -> Invalid("Page count not updated")
                    psInfo.pages < psInfo.colourPages -> Invalid("Page count less than colour page count")
                    !file.isFile -> Invalid("File was not created")
                    else -> Valid
                }
            }
        }

        private val validator = validate(Printer.hasSufficientQuota, testValidator)

        @AfterClass
        @JvmStatic
        fun after() {
            println("\n\n")
            log.info("Cleaning up")
            val count = PrintJobs.purge(0) // purge all files
            log.info("Purged $count")
        }
    }

    @Test
    fun printSingleJob() {
        val result = Printer.print("Single Job", testUser, testQueue, testPs.inputStream(), validator)
        if (result !is PrintInfo)
            fail("Print response was an error: $result")
        concurrentTest { callback ->
            watch(result.id, callback) { _, current ->
                log.info("Stage $current")
            }
        }
        val printed = result.assertPrinted(log)

        val jobs = transaction {
            DestinationDb[printed.destination].printJobs(5).map(PrintJobDb::toJson)
        }

        assertTrue(jobs.size == 1, "Only one job has been printed")
        with (jobs.first()) {
            assertEquals(result.name, name)
            assertEquals(result.shortUser, shortUser)
            assertEquals(result.id, id)
        }
    }

    @Test
    fun insufficientQuota() {
        val testUser = "test1"
        assertEquals(1, Configs.baseQuota(testUser), "Quota parsing failed")
        val result = Printer.print("Single Job", testUser, testQueue, testPs.inputStream(), validator)
        if (result !is PrintInfo)
            fail("Print response was an error: $result")
        concurrentTest { callback ->
            watch(result.id, callback) { _, current ->
                log.info("Stage $current")
            }
        }
        result.assertFailed(log, Printer.INSUFFICIENT_QUOTA)
    }

}