package ca.mcgill.science.ctf.tepid.server.tables

import ca.mcgill.science.ctf.tepid.server.Configs
import ca.mcgill.science.ctf.tepid.server.internal.*
import org.junit.Test
import kotlin.test.assertEquals
import kotlin.test.assertFalse
import kotlin.test.assertNull
import kotlin.test.assertTrue

class DestinationTest {

    init {
        TestConfigs.setup()
    }

    @Test
    fun markUpDown() {

        testTransaction {
            val d = DestinationDb.new(Configs.generateId()) {
                name = "Destination Test Up Down"
                queue = QueueDb[testQueue]
                up = true
            }

            val markdown = "Test markdown"

            val d1 = d.toJson()
            d.markUp()
            val d2 = d.toJson()
            assertTrue(d1.up)
            assertNull(d1.ticket)
            assertEquals(d1, d2, "Up should be idempotent")
            d.markDown(testUser to "$markdown 1")
            val d3 = d.toJson()
            assertFalse(d3.up)
            assertEquals(testUser, d3.ticket?.shortUser)
            assertEquals("$markdown 1", d3.ticket?.message)
            d.markDown(testUser to "$markdown 2")
            val d4 = d.toJson()
            assertEquals(testUser, d4.ticket?.shortUser)
            assertEquals("$markdown 2", d4.ticket?.message)
            val tickets = d.ticketHistory(10)
            assertTrue(tickets.size == 2, "${d.name} should only have 2 tickets")
            assertEquals("$markdown 2", tickets[0].message, "Tickets not sorted by descending creation date")
            assertEquals("$markdown 1", tickets[1].message, "Tickets not sorted by descending creation date")
        }

    }

    @Test
    fun deletion() {
        testTransaction {
            assertEquals(0, DestinationTicketDb.count())
            DestinationDb[testDestination].markDown(testUser to "Deletion test")
            assertEquals(1, DestinationDb.count())
            DestinationDb[testDestination].markUp()
            assertEquals(0, DestinationTicketDb.count())
        }
    }

}