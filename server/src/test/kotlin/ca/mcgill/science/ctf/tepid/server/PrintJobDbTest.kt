package ca.mcgill.science.ctf.tepid.server

import ca.mcgill.science.ctf.tepid.bindings.defaultTimeStamp
import ca.mcgill.science.ctf.tepid.server.internal.TestConfigs
import ca.mcgill.science.ctf.tepid.server.internal.testDestination
import ca.mcgill.science.ctf.tepid.server.internal.testTransaction
import ca.mcgill.science.ctf.tepid.server.internal.testUser
import ca.mcgill.science.ctf.tepid.server.tables.PrintJobDb
import ca.mcgill.science.ctf.tepid.server.tables.PrintJobs
import ca.mcgill.science.ctf.tepid.server.utils.PsInfo
import org.junit.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue
import kotlin.test.fail

class PrintJobDbTest {

    init {
        TestConfigs.setup()
    }

    @Test
    fun createAndFetch(): Unit = testTransaction {
        val job = PrintJobDb.new("testId") {
            name = "testName"
            shortUser = testUser
        }
        PrintJobDb.findById(job.id)?.apply {
            assertTrue(System.currentTimeMillis() - created < 1000, "Created timestamp is not accurate")
            val data = listOf(
                    "testId" to id.value,
                    "testName" to name,
                    testUser to shortUser,
                    Configs.tmpDir.resolve("${job.id.value}.ps.xz").absolutePath to file.absolutePath,
                    defaultTimeStamp to received,
                    defaultTimeStamp to processed,
                    defaultTimeStamp to printed,
                    defaultTimeStamp to failed,
                    null to error
            )

            data.forEachIndexed { index, (expected, result) ->
                assertEquals(expected, result, "PrintJob match failed at index $index")
            }
        } ?: fail("Could not find print job '${job.id}'")
    }

    @Test
    fun totalQuotaUsed() = testTransaction {

        fun assertQuotaEquals(expected: Int, message: String) =
                assertEquals(expected, PrintJobs.getTotalQuotaUsed(testUser), message)

        assertQuotaEquals(0, "User with no print jobs should have 0 quota used")

        // add printed job 1

        val job1 = PrintJobDb.new {
            name = "Job 1"
            shortUser = testUser
        }

        job1.printed(testDestination, 35)

        assertQuotaEquals(35, "Quota used did not update after printed job")

        // add non printed job 2

        val job2 = PrintJobDb.new {
            name = "Job 2"
            shortUser = testUser
        }

        job2.processed(PsInfo(10, 0))

        assertQuotaEquals(35, "Quota used should not change if print job that isn't printed is added")

        // print job 2

        job2.printed(testDestination, 10)

        assertQuotaEquals(45, "Quota used should be 35 + 10")

        // refund job 2

        job2.refund()

        assertQuotaEquals(35, "Quota used should not include refunded jobs")

        // switch user for job 1

        job1.shortUser = "${testUser}2"

        assertQuotaEquals(0, "Quota used should not include jobs from other users")
    }

}