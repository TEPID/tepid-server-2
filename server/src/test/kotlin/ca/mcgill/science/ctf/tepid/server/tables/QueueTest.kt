package ca.mcgill.science.ctf.tepid.server.tables

import ca.mcgill.science.ctf.tepid.server.internal.TestConfigs
import ca.mcgill.science.ctf.tepid.server.internal.testDestination
import ca.mcgill.science.ctf.tepid.server.internal.testQueue
import ca.mcgill.science.ctf.tepid.server.internal.testTransaction
import org.junit.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue

class QueueTest {

    init {
        TestConfigs.setup()
    }

    @Test
    fun json() {
        testTransaction {
            with(QueueDb[testQueue].toJson()) {
                assertEquals(testQueue, id)
                assertEquals("default", loadBalancer)
                assertTrue(destinations.size == 1, "Should have one destination")
                with(destinations.first()) {
                    assertEquals(testDestination, id)
                }
            }
        }
    }

    @Test
    fun deletion() {
        testTransaction {
            assertEquals(1, DestinationDb.count())
            QueueDb[testQueue].delete()
            assertEquals(0, DestinationDb.count())
        }
    }

}