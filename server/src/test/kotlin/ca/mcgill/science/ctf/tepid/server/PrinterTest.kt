package ca.mcgill.science.ctf.tepid.server

import ca.allanwang.kit.logger.WithLogging
import ca.mcgill.science.ctf.tepid.models.Created
import ca.mcgill.science.ctf.tepid.models.Printed
import ca.mcgill.science.ctf.tepid.models.Processed
import ca.mcgill.science.ctf.tepid.models.Received
import ca.mcgill.science.ctf.tepid.server.internal.*
import ca.mcgill.science.ctf.tepid.server.tables.PrintJobDb
import ca.mcgill.science.ctf.tepid.server.utils.Printer
import ca.mcgill.science.ctf.tepid.server.utils.PsInfo
import ca.mcgill.science.ctf.tepid.server.utils.copyFrom
import org.jetbrains.exposed.sql.transactions.transaction
import org.junit.Test
import java.io.File
import kotlin.test.assertNull
import kotlin.test.assertTrue
import kotlin.test.fail

class PrinterTest : WithLogging() {

    init {
        TestConfigs.setup()
    }

    private fun testPrintThread(id: String, pageCount: Int, error: String? = null): Thread {
        val job = transaction {
            PrintJobDb.new(id) {
                name = "Job $id"
                shortUser = testUser
            }
        }
        log.info("Created ${job.id}")
        return Thread({
            sleep(500, 800)
            job.received()
            log.info("Received $id")
            sleep(500, 800)
            job.processed(PsInfo(pageCount, 0))
            log.info("Processed $id")
            sleep(700, 800)
            if (error == null) {
                job.printed(testDestination, pageCount)
                log.info("Printed $id")
            } else {
                job.failed(error)
                log.info("Failed $id")
            }
        }, "Test Print $id")
    }

    private fun watch(id: String, callback: CompletableCallback) = watch(id, callback) { prev, current ->
        // given adequate sleep times, we should be capturing all stages properly
        when (current) {
            is Created -> assertNull(prev)
            is Received -> assertTrue(prev is Created)
            is Processed -> assertTrue(prev is Received)
            is Printed -> assertTrue(prev is Processed)
        }
        log.info("Stage $current")
    }

    @Test
    fun watcher() {
        testTransaction(defaultSetup = true) { }
        concurrentTest { result ->
            val thread = testPrintThread("job1", 8)
            watch("job1", result)
            thread.start()
        }
    }

    @Test
    fun noDecompression() {
        val source = testPs
        val dest = File.createTempFile("tepid-test-nd", "ps")
        dest.copyFrom(Printer.decompress(source))
        val equal = source.length() == dest.length() &&
                source.inputStream().bufferedReader().readLines() == dest.inputStream().bufferedReader().readLines()
        dest.delete()
        assertTrue(equal, "Decompressing non xz formatted file did not result in exact duplicate")
    }

    @Test
    fun compression() {
        val source = testPs
        val tmp = File.createTempFile("tepid-test-c", "ps")
        try {
            source.inputStream().use { input ->
                Printer.compress(tmp.outputStream()).use { output ->
                    input.copyTo(output)
                }
            }
            assertTrue(tmp.length() > 0 && tmp.length() < source.length(), "Compressed file was not smaller")
        } catch (e: Exception) {
            fail(e.message)
        } finally {
            tmp.delete()
        }
    }
}