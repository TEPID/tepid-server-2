package ca.mcgill.science.ctf.tepid.server.internal

import ca.mcgill.science.ctf.tepid.models.Failed
import ca.mcgill.science.ctf.tepid.models.PrintStage
import ca.mcgill.science.ctf.tepid.models.Printed
import ca.mcgill.science.ctf.tepid.server.models.PrintInfo
import ca.mcgill.science.ctf.tepid.server.tables.DestinationDb
import ca.mcgill.science.ctf.tepid.server.tables.PrintJobDb
import ca.mcgill.science.ctf.tepid.server.tables.PrintJobs
import ca.mcgill.science.ctf.tepid.server.tables.QueueDb
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers
import org.apache.logging.log4j.Logger
import org.jetbrains.exposed.sql.SchemaUtils.create
import org.jetbrains.exposed.sql.SchemaUtils.drop
import org.jetbrains.exposed.sql.StdOutSqlLogger
import org.jetbrains.exposed.sql.Table
import org.jetbrains.exposed.sql.Transaction
import org.jetbrains.exposed.sql.deleteAll
import org.jetbrains.exposed.sql.transactions.transaction
import java.io.File
import java.util.concurrent.ThreadLocalRandom
import java.util.concurrent.TimeUnit
import kotlin.test.assertEquals
import kotlin.test.fail

fun <T> testTransaction(log: Boolean = true,
                        tables: Array<out Table> = TestConfigs.tables,
                        defaultSetup: Boolean = true,
                        statement: Transaction.() -> T): T = try {
    transaction {
        tables.forEach { it.deleteAll() }
        if (defaultSetup) {
            val queue = QueueDb.new(testQueue) {
                name = "Default Test Queue"
            }
            DestinationDb.new(testDestination) {
                name = "Default Destination"
                this.queue = queue
                up = true
            }
        }
        if (log)
            stdlog()
        try {
            statement()
        } catch (e: Exception) {
            fail("Test statement failed: ${e.message}")
        }
    }
} catch (e: Exception) {
    fail("Test Transaction failed: ${e.message}")
}

fun Transaction.stdlog() = logger.addLogger(StdOutSqlLogger)

fun sleep(min: Int, max: Int) {
    val duration = ThreadLocalRandom.current().nextInt(max - min) + min
    Thread.sleep(duration.toLong())
}

const val testUser = "test100"
const val testDestination = "destination1"
const val testQueue = "queue1"

fun resource(path: String) = File(TestConfigs::class.java.classLoader.getResource(path).file)

val testPs = resource("ps/test.ps")

/**
 * Watch job on a separate thread, locked by the [callback]
 */
inline fun watch(id: String, callback: CompletableCallback, crossinline action: (prev: PrintStage?, current: PrintStage) -> Unit) {
    var stage: PrintStage? = null
    PrintJobs.watch(id).timeout(10, TimeUnit.SECONDS).subscribeOn(Schedulers.io()).subscribeBy(
            onNext = {
                action(stage, it)
                stage = it
            },
            onComplete = {
                callback.onComplete()
            },
            onError = {
                callback.onError(it)
            }
    )
}

fun PrintInfo.assertPrinted(logger: Logger? = null): Printed {
    val stage = transaction {
        PrintJobDb[id].toJson().stage
    } as? Printed ?: fail("Job did not print")
    if (logger != null) {
        logger.info("Sent $name to ${stage.destination}")
    }
    return stage
}

fun PrintInfo.assertFailed(logger: Logger? = null, message: String? = null): Failed {
    val stage = transaction {
        PrintJobDb[id].toJson().stage
    } as? Failed ?: fail("Job did not fail")
    if (message != null)
        assertEquals(message, stage.message, "Message mismatch")
    if (logger != null) {
        logger.info("Job $name failed: ${stage.message}")
    }
    return stage
}