package ca.mcgill.science.ctf.tepid.server.models

import ca.mcgill.science.ctf.tepid.server.Configs
import ca.mcgill.science.ctf.tepid.server.tables.PrintJobs
import ca.mcgill.science.ctf.tepid.server.utils.Printer
import ca.mcgill.science.ctf.tepid.server.utils.PsInfo
import java.io.File

/**
 * Immutable data snapshot after a job is processed
 */
data class PrintCandidate(
        val id: String,
        val name: String,
        val shortUser: String,
        val psInfo: PsInfo)

/**
 * Return type for [Printer.print]
 */
sealed class PrintResponse

/**
 * Basic job info before processing
 */
data class PrintInfo(
        val id: String,
        val name: String,
        val shortUser: String) : PrintResponse()

data class PrintError(
        val message: String,
        val timeStamp: Long = System.currentTimeMillis()) : PrintResponse()

/**
 * Contains all the necessary info to print an actual job
 *
 * [job] attributes for the print job
 * [file] location of temp file to print
 * [destination] unique identifier for destination to print to
 */
data class PrintRequest(val job: PrintCandidate,
                        val file: File,
                        val destination: String) {

    inline val psInfo: PsInfo
        get() = job.psInfo

    /**
     * The quota amount that will be deducted if this page prints
     */
    inline val quotaCost: Int
        get() = psInfo.cost

    /**
     * Fetches quota from database and check if it is greater than the cost
     */
    fun hasSufficientQuota(): Boolean {
        val baseQuota = Configs.baseQuota(job.shortUser)
        val quotaUsed = PrintJobs.getTotalQuotaUsed(job.shortUser)
        return hasSufficientQuota(baseQuota - quotaUsed)
    }

    fun hasSufficientQuota(quota: Int): Boolean = quotaCost < quota
}
