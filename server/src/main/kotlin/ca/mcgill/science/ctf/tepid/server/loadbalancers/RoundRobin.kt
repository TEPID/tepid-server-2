package ca.mcgill.science.ctf.tepid.server.loadbalancers

import ca.mcgill.science.ctf.tepid.server.models.PrintCandidate
import ca.mcgill.science.ctf.tepid.server.models.PrintRequest
import ca.mcgill.science.ctf.tepid.server.utils.LoadBalancer
import java.util.concurrent.atomic.AtomicReference

/**
 * Loops through candidates by index to assign the next job
 * No consideration is done with regards to the actual job or page count
 *
 * If the old index cannot be found, it starts back at candidate 0
 */
class RoundRobin : LoadBalancer {

    private val lastUsed = AtomicReference<String>("")

    override fun select(candidates: List<String>, candidate: PrintCandidate): String {
        val oldIndex = candidates.indexOf(lastUsed.get()) // -1 if not found
        return candidates[(oldIndex + 1) % candidates.size]
    }

    override fun register(request: PrintRequest) {
        lastUsed.set(request.destination)
    }
}