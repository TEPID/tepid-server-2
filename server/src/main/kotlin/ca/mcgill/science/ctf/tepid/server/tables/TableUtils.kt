package ca.mcgill.science.ctf.tepid.server.tables

import org.jetbrains.exposed.sql.*

interface WithCreation {
    val created: Column<Long>
}

fun <T> T.takeByCreation(count: Int, order: SortOrder = SortOrder.DESC, where: SqlExpressionBuilder.() -> Op<Boolean>): Query
        where T : Table, T : WithCreation = select(where).limit(count).orderBy(created to order)