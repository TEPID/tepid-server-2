package ca.mcgill.science.ctf.tepid.server.tables

import ca.mcgill.science.ctf.tepid.models.DestinationJson
import ca.mcgill.science.ctf.tepid.server.Configs
import ca.mcgill.science.ctf.tepid.server.models.JacksonModel
import org.jetbrains.exposed.dao.Entity
import org.jetbrains.exposed.dao.EntityClass
import org.jetbrains.exposed.dao.EntityID
import org.jetbrains.exposed.dao.IdTable
import org.jetbrains.exposed.sql.ReferenceOption
import org.jetbrains.exposed.sql.SizedIterable
import org.jetbrains.exposed.sql.transactions.transaction

object Destinations : IdTable<String>() {

    override val id = varchar("id", 128).primaryKey().clientDefault { Configs.generateId() }.entityId()

    val name = varchar("name", 128)

    /**
     * Queue identifier (used to group destinations)
     */
    val queue = reference("queue", Queues, ReferenceOption.CASCADE)
    /**
     * Whether or not the printer is up and running
     */
    val up = bool("up").default(false)

}

class DestinationDb(id: EntityID<String>) : Entity<String>(id), JacksonModel<DestinationJson> {
    companion object : EntityClass<String, DestinationDb>(Destinations)

    var name by Destinations.name
    var up by Destinations.up
    var queue by QueueDb referencedOn Destinations.queue
    val ticket: DestinationTicketDb?
        get() = if (!up) ticketHistory(1).firstOrNull() else null

    fun ticketHistory(take: Int) = transaction {
        DestinationTicketDb.wrapRows(DestinationTickets.takeByCreation(take)
        { DestinationTickets.destination eq id }).toList()
    }

    fun markUp(shortUser: String?): Unit = transaction {
        ticket?.resolve(shortUser)
        up = true
    }

    fun markDown(ticket: Pair<String, String>?): Unit = transaction {
        up = false
        if (ticket != null)
            DestinationTicketDb.new(Configs.generateId()) {
                shortUser = ticket.first
                message = ticket.second
                destination = this@DestinationDb.id
            }
    }

    fun printJobs(take: Int) = transaction {
        PrintJobDb.wrapRows(PrintJobs.takeByCreation(take)
        { PrintJobs.destination eq id }).toList()
    }

    override fun toJson(): DestinationJson = transaction {
        DestinationJson(id.value, name, ticket?.toJson(), up)
    }
}

