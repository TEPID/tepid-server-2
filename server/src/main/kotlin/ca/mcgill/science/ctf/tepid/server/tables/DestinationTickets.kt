package ca.mcgill.science.ctf.tepid.server.tables

import ca.mcgill.science.ctf.tepid.bindings.defaultTimeStamp
import ca.mcgill.science.ctf.tepid.models.TicketJson
import ca.mcgill.science.ctf.tepid.server.Configs
import ca.mcgill.science.ctf.tepid.server.models.JacksonModel
import org.jetbrains.exposed.dao.Entity
import org.jetbrains.exposed.dao.EntityClass
import org.jetbrains.exposed.dao.EntityID
import org.jetbrains.exposed.dao.IdTable
import org.jetbrains.exposed.sql.ReferenceOption
import org.jetbrains.exposed.sql.transactions.transaction

object DestinationTickets : IdTable<String>(), WithCreation {
    override val id = varchar("id", 128).primaryKey().clientDefault { Configs.generateId() }.entityId()

    override val created = long("created").clientDefault { System.currentTimeMillis() }
    val shortUser = varchar("short_user", 64)
    val destination = reference("destination", Destinations, ReferenceOption.CASCADE)
    val message = varchar("message", 256)
    val resolved = long("resolved").default(defaultTimeStamp)
    val resolvedBy = varchar("resolved_by", 64).nullable()
}

class DestinationTicketDb(id: EntityID<String>) : Entity<String>(id), JacksonModel<TicketJson> {
    companion object : EntityClass<String, DestinationTicketDb>(DestinationTickets)

    var created by DestinationTickets.created
    var shortUser by DestinationTickets.shortUser
    var destination by DestinationTickets.destination
    var message by DestinationTickets.message
    var resolved by DestinationTickets.resolved
    var resolvedBy by DestinationTickets.resolvedBy

    fun resolve(shortUser: String?): Unit = transaction {
        resolved = System.currentTimeMillis()
        resolvedBy = shortUser
    }

    override fun toJson(): TicketJson = transaction {
        TicketJson(id.value, message, shortUser, created)
    }
}