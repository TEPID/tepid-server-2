package ca.mcgill.science.ctf.tepid.server.tables

import ca.allanwang.kit.logger.Loggable
import ca.allanwang.kit.logger.WithLogging
import ca.allanwang.kit.rx.RxWatcher
import ca.mcgill.science.ctf.tepid.bindings.defaultTimeStamp
import ca.mcgill.science.ctf.tepid.models.NotFound
import ca.mcgill.science.ctf.tepid.models.PrintJobJson
import ca.mcgill.science.ctf.tepid.models.PrintStage
import ca.mcgill.science.ctf.tepid.server.Configs
import ca.mcgill.science.ctf.tepid.server.models.JacksonModel
import ca.mcgill.science.ctf.tepid.server.models.PrintCandidate
import ca.mcgill.science.ctf.tepid.server.models.PrintInfo
import ca.mcgill.science.ctf.tepid.server.models.PrintRequest
import ca.mcgill.science.ctf.tepid.server.utils.Printer
import ca.mcgill.science.ctf.tepid.server.utils.PsInfo
import io.reactivex.Observable
import org.jetbrains.exposed.dao.Entity
import org.jetbrains.exposed.dao.EntityClass
import org.jetbrains.exposed.dao.EntityID
import org.jetbrains.exposed.dao.IdTable
import org.jetbrains.exposed.sql.ReferenceOption
import org.jetbrains.exposed.sql.and
import org.jetbrains.exposed.sql.select
import org.jetbrains.exposed.sql.transactions.transaction
import org.jetbrains.exposed.sql.update
import java.io.File

interface PrintJobsContract {
    /**
     * Remove all files older than [expiration] ms
     * Defaults to [Configs.expiration]
     * Those that are purged will be flagged as [PrintJobs.deleted]
     * Returns number of rows updated
     */
    fun purge(expiration: Long = Configs.expiration): Int

    /**
     * Gets the total quota cost used by the short user
     * Jobs counted are those that have printed and are not refunded
     */
    fun getTotalQuotaUsed(shortUser: String): Int

    /**
     * Returns an observable that emits the print stages of the job with the supplied [id]
     * See [PrintStage] for class options
     * Emissions are polled every [Configs.jobWatcherFrequency] ms
     */
    fun watch(id: String): Observable<PrintStage>
}

object PrintJobs : IdTable<String>(), PrintJobsContract, WithCreation, Loggable by WithLogging("PrintJobs") {

    override val id = varchar("id", 128).primaryKey().clientDefault { Configs.generateId() }.entityId()

    val name = varchar("name", 128)
    val shortUser = varchar("short_user", 64)

    /**
     * Directory for file
     * Taken from [Configs.tmpDir]
     * Note that the full file path is
     */
    val fileDir = varchar("file", 128).clientDefault { Configs.tmpDir.absolutePath }

    /**
     * Destination to which the job should be printed to
     * Initialized if [printed]
     */
    val destination = reference("destination", Destinations, ReferenceOption.CASCADE).nullable()
    /**
     * Total page count of the job
     * Initialized if [processed]
     */
    val pageCount = integer("page_count").default(0)
    /**
     * Total colour page count of the job
     * Initialized if [processed]
     */
    val colourPageCount = integer("colour_page_count").default(0)
    /**
     * Total cost of the job based on [Configs.colourPageValue]
     * Initialized if [printed]
     */
    val quotaCost = integer("quota_cost").default(0)
    /**
     * Boolean denoting whether [quotaCost] should be considered in the usage sum
     */
    val refunded = bool("refunded").default(false)

    /*
     * Time stamps
     */
    override val created = long("created").clientDefault { System.currentTimeMillis() }
    val received = long("received").default(defaultTimeStamp)
    val processed = long("processed").default(defaultTimeStamp)
    val printed = long("printed").default(defaultTimeStamp)
    val failed = long("failed").default(defaultTimeStamp)
    val deleted = long("deleted").default(defaultTimeStamp)


    /**
     * Print job error message
     * Initialized if [failed]
     * See [Printer] for some error string constants
     */
    val error = varchar("error", 128).nullable()

    override fun purge(expiration: Long): Int {
        val now = System.currentTimeMillis()
        val purgeTime = now - expiration
        val files = transaction {
            select { (created lessEq purgeTime) and (deleted eq defaultTimeStamp) }.map {
                it[fileDir]
            }
        }
        files.map(::File).filter(File::isFile).forEach { it.delete() }
        val updateCount = transaction {
            update({ (created lessEq purgeTime) and (deleted eq defaultTimeStamp) }) {
                it[deleted] = now
            }
        }
        if (updateCount != files.size)
            log.warn("Update count $updateCount does not match purge size ${files.size}")
        return updateCount
    }


    override fun getTotalQuotaUsed(shortUser: String): Int = transaction {
        select { (PrintJobs.shortUser eq shortUser) and (printed neq defaultTimeStamp) and (refunded eq false) }.sumBy { it[quotaCost] }
    }

    private val watcher: PrintStageWatcher by lazy { PrintStageWatcher() }

    override fun watch(id: String) = watcher.watch(id)
}

class PrintJobDb(id: EntityID<String>) : Entity<String>(id), JacksonModel<PrintJobJson> {
    companion object : EntityClass<String, PrintJobDb>(PrintJobs)

    var name by PrintJobs.name
    var shortUser by PrintJobs.shortUser
    val file by lazy {
        transaction {
            File(PrintJobs.fileDir.lookup()).resolve("$id.ps.xz")
        }
    }
    var destination by DestinationDb optionalReferencedOn PrintJobs.destination
    var pageCount by PrintJobs.pageCount
    var colourPageCount by PrintJobs.colourPageCount
    var quotaCost by PrintJobs.quotaCost
    var refunded by PrintJobs.refunded

    var created by PrintJobs.created
    var received by PrintJobs.received
    var processed by PrintJobs.processed
    var printed by PrintJobs.printed
    var failed by PrintJobs.failed
    var deleted by PrintJobs.deleted
    var error by PrintJobs.error

    fun received(): Unit = transaction {
        received = System.currentTimeMillis()
    }

    fun processed(psInfo: PsInfo): PrintCandidate = transaction {
        psInfo.validate()
        processed = System.currentTimeMillis()
        pageCount = psInfo.pages
        colourPageCount = psInfo.colourPages
        PrintCandidate(id.value, name, shortUser, psInfo)
    }

    fun printed(request: PrintRequest) = printed(request.destination, request.quotaCost)

    fun printed(destination: String, quotaCost: Int): Unit = transaction {
        this@PrintJobDb.printed = System.currentTimeMillis()
        this@PrintJobDb.destination = DestinationDb[destination]
        this@PrintJobDb.quotaCost = quotaCost
    }

    fun failed(message: String): Unit = transaction {
        failed = System.currentTimeMillis()
        error = message
    }

    fun refund(refunded: Boolean = true): Unit = transaction {
        this@PrintJobDb.refunded = refunded
    }

    fun info(): PrintInfo = transaction {
        PrintInfo(id.value, name, shortUser)
    }

    inline val stage: PrintStage
        get() = toJson().stage

    fun watch() = PrintJobs.watch(id.value)

    override fun toJson(): PrintJobJson = transaction {
        PrintJobJson(id.value, name, shortUser, destination?.id?.value,
                pageCount, colourPageCount, quotaCost,
                refunded, created, received, processed, printed, failed, deleted, error)
    }
}

class PrintStageWatcher(
        override val pollingInterval: Long = Configs.jobWatcherFrequency
) : RxWatcher<String, PrintStage>() {
    override val timeoutDuration: Long = 120000L

    override fun emit(id: String): PrintStage = transaction { PrintJobDb.findById(id)?.stage ?: NotFound }

    override fun isCompleted(id: String, value: PrintStage): Boolean = value.finished
}