package ca.mcgill.science.ctf.tepid.server.models

import ca.mcgill.science.ctf.tepid.bindings.TepidJackson

interface JacksonModel<out T : TepidJackson> {

    fun toJson(): T

}