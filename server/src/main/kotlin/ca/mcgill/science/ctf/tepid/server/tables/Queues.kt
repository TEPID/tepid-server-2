package ca.mcgill.science.ctf.tepid.server.tables

import ca.allanwang.kit.logger.Loggable
import ca.allanwang.kit.logger.WithLogging
import ca.mcgill.science.ctf.tepid.models.QueueJson
import ca.mcgill.science.ctf.tepid.server.Configs
import ca.mcgill.science.ctf.tepid.server.models.JacksonModel
import ca.mcgill.science.ctf.tepid.server.utils.LoadBalancer
import ca.mcgill.science.ctf.tepid.server.utils.LoadBalancers
import org.jetbrains.exposed.dao.Entity
import org.jetbrains.exposed.dao.EntityClass
import org.jetbrains.exposed.dao.EntityID
import org.jetbrains.exposed.dao.IdTable
import org.jetbrains.exposed.sql.transactions.transaction

interface QueuesContract {
    /**
     * Save the new load balancer in the db if it's valid and reload it
     * Returns [true] if the load balancer has changed, and [false] otherwise
     */
    fun updateLoadBalancer(id: String, loadBalancer: String): Boolean
}

object Queues : IdTable<String>(), QueuesContract, Loggable by WithLogging("Queues") {

    override val id = varchar("id", 128).primaryKey().clientDefault { Configs.generateId() }.entityId()

    val name = varchar("name", 128)

    val loadBalancer = varchar("load_balancer", 32).default(LoadBalancer.DEFAULT)

    override fun updateLoadBalancer(id: String, loadBalancer: String): Boolean {
        val queue = transaction {
            QueueDb.findById(id)
        }
        if (queue == null) {
            log.error("Could not find queue $id; failed to update load balancer")
            return false
        }
        val newBalancer = Configs.loadBalancer(loadBalancer)
        if (newBalancer == null) {
            log.error("Could not find load balancer $loadBalancer for queue $id; aborting update")
            return false
        }
        transaction {
            queue.loadBalancer = loadBalancer
        }
        LoadBalancers.update(id, newBalancer)
        return true
    }
}

class QueueDb(id: EntityID<String>) : Entity<String>(id), JacksonModel<QueueJson> {
    companion object : EntityClass<String, QueueDb>(Queues)

    var name by Queues.name
    var loadBalancer by Queues.loadBalancer
    val destinations by DestinationDb referrersOn Destinations.queue

    override fun toJson(): QueueJson = transaction {
        QueueJson(id.value, name, loadBalancer, destinations.map(DestinationDb::toJson))
    }
}