package ca.mcgill.science.ctf.tepid.bindings

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonInclude

/**
 * Created by Allan Wang on 2017-10-29.
 *
 * Jackson binding annotations
 */

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
interface TepidJackson

const val defaultTimeStamp = -1L