package ca.mcgill.science.ctf.tepid.models

import ca.mcgill.science.ctf.tepid.bindings.TepidJackson
import ca.mcgill.science.ctf.tepid.bindings.defaultTimeStamp
import com.fasterxml.jackson.annotation.JsonIgnore

data class PrintJobJson(
        val id: String,
        val name: String,
        val shortUser: String,
        val destination: String?,
        val pageCount: Int,
        val colourPageCount: Int,
        val quotaCost: Int,
        val refunded: Boolean,
        val created: Long,
        val received: Long,
        val processed: Long,
        val printed: Long,
        val failed: Long,
        val deleted: Long,
        val error: String?
) : TepidJackson {

    @get:JsonIgnore
    val stage: PrintStage by lazy {
        when {
            failed != defaultTimeStamp -> Failed(failed, error!!)
            printed != defaultTimeStamp -> Printed(printed, destination!!, pageCount, colourPageCount)
            processed != defaultTimeStamp -> Processed(processed)
            received != defaultTimeStamp -> Received(received)
            created != defaultTimeStamp -> Created(created)
            else -> NotFound
        }
    }

}

/**
 * Stage info for a given print job
 * [finished] means that the stage will no longer change
 */
sealed class PrintStage(val finished: Boolean)

data class Created(val time: Long) : PrintStage(false)
data class Processed(val time: Long) : PrintStage(false)
data class Received(val time: Long) : PrintStage(false)
data class Printed(val time: Long, val destination: String, val pageCount: Int, val colourPageCount: Int) : PrintStage(true)
data class Failed(val time: Long, val message: String) : PrintStage(true)
object NotFound : PrintStage(true) {
    override fun toString(): String = "NotFound"
}