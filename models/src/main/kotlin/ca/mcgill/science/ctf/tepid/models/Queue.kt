package ca.mcgill.science.ctf.tepid.models

import ca.mcgill.science.ctf.tepid.bindings.TepidJackson

data class QueueJson(
        val id: String,
        val name: String,
        val loadBalancer: String,
        val destinations: List<DestinationJson>
) : TepidJackson