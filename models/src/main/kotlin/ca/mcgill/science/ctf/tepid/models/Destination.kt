package ca.mcgill.science.ctf.tepid.models

import ca.mcgill.science.ctf.tepid.bindings.TepidJackson

data class DestinationJson(
        val id: String,
        val name: String,
        val ticket: TicketJson?,
        val up: Boolean
) : TepidJackson