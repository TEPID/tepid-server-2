package ca.mcgill.science.ctf.tepid.models

import ca.mcgill.science.ctf.tepid.bindings.TepidJackson

data class TicketJson(
        val id: String,
        val message: String,
        val shortUser: String,
        val created: Long
) : TepidJackson