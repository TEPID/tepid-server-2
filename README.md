# Tepid Server

Tepid is an extensible printing service used backed by SQL, with Jackson data models.

## Work Flow

* User sends job (input stream) with some identifiers and a desired queue (see `Printer.print`)
* Tepid checks queues for candidate destinations and picks one using a load balancer; progress is updated in the db
* Some verifications are done, and if everything is okay, a job is sent to the printer

Since there are a lot of points (such as printer commands and base quota calculation) that are specific to the implementor,
Tepid is highly functional, and offers several endpoints in its `Configs.kt` object.
It must be configured through the `Tepid.kt` object at least once before usage.

## Extra Features

* Tracking printer tickets when marking them as down
* Tracking quota used by an individual
* Retrieving job lists sorted by creation date

## Models

Typically, a data model is created in three parts:

* Table - Used to create and manage the SQL columns. 
For the most part, tables have a unique id, generated through `Configs`
* Entity - Used to interact with individual rows in tables.
Comes with a bunch of lazy functions to retrieve data from the database.
* JSON - Jackson ready models available for serialization. 
All methods in those classes are guaranteed to work with the supplied arguments,
making them usable from both the server and the client. Most JSON models are mapped from entities,
and can be converted back to entities by referencing the id.